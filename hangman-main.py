import random
import os
import sys

import subprocess, platform

list_hangman = ['''
     |
     |
     |
     |
     |
     |
 xiu_|___''', '''
      _______
     |/      |
     |
     |
     |
     |
     |
 xiu_|___''', '''
      _______
     |/      |
     |      (_)
     |
     |
     |
     |
 xiu_|___''', '''
      _______
     |/      |
     |      (_)
     |       |
     |       |
     |
     |
 xiu_|___''', '''
      _______
     |/      |
     |      (_)
     |      \|/
     |       |
     |
     |
 xiu_|___''', '''
      _______
     |/      |
     |      (_)
     |      \|/
     |       |
     |      / \\
     |
 xiu_|___''', '''
      _______
     |/      |
     |      (x)
     |      \|/
     |       |
     |      / \\
     |
 xiu_|___''']

chosen_word = input("Type in the Jackass word: ")
os.system('clear')

underscore_word_list = ["_" for char in chosen_word]
underscore_word = ''.join(underscore_word_list)

sys.stderr.write("\x1b[2J\x1b[H")

print(underscore_word)
number_hangman = 0
wrong_list = []

while list_hangman[number_hangman] != list_hangman[6] and underscore_word != chosen_word:
    correctness = 0
    chosen_char = input("Type in your chosen character: ")
    
    def check_coincide():
        for char in wrong_list:
            if chosen_char == char:
                return True
        return False
    
    for number, char in enumerate(chosen_word):
        if chosen_char == underscore_word_list[number]:
            chosen_char = input("It has been chosen, choose another character!: ")
            break
        elif wrong_list and check_coincide():
            chosen_char = input("It has been chosen, choose another character!: ")
            break
        elif chosen_char == char:
            underscore_word_list[number] = chosen_char
            underscore_word = ''.join(underscore_word_list)
            print(underscore_word)
            correctness += 1
    
    if correctness == 0:
        number_hangman += 1
        print(list_hangman[number_hangman])
    else:
        print("You are right!")

if list_hangman[number_hangman] == list_hangman[6]:
    print("You lose")
else:
    print("You win")

